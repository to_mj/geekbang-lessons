/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.geekbang.thinking.in.spring.ioc.overview.dependency.injection;

import lombok.extern.slf4j.Slf4j;
import org.geekbang.thinking.in.spring.ioc.overview.repository.UserRepository;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 依赖注入示例
 *
 * @author <a href="mailto:mercyblitz@gmail.com">Mercy</a>
 * @since
 */
@Slf4j
public class DependencyInjectionDemo {

    /**
     * ClassPathXmlApplicationContext
     * userRepository.getBeanFactory()
     * userRepository.getObjectFactory().getObject()
     * <p>
     * 这三者的区别
     * 1. BeanFactory
     * 2. DefaultListableBeanFactory
     * 3. ObjectFactory
     * <p>
     * 依赖查找、依赖注入的区别
     * <p>
     * 注意:依赖查找、依赖注入的来源并不是同一个
     *
     * @param args
     */
    public static void main(String[] args) {
        // 配置 XML 配置文件
        // 启动 Spring 应用上下文
        BeanFactory beanFactory = new ClassPathXmlApplicationContext("classpath:/META-INF/dependency-injection-context.xml");

//        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:/META-INF/dependency-injection-context.xml");

        // 依赖来源一：自定义 Bean
        UserRepository userRepository = beanFactory.getBean("userRepository", UserRepository.class);

        log.info("beanFactory: {}", beanFactory);
        log.info("userRepository.getBeanFactory(): {}", userRepository.getBeanFactory());
        log.info("userRepository.getObjectFactory().getObject(): {}", userRepository.getObjectFactory().getObject());

        log.info("beanFactory == userRepository.getBeanFactory() ? {}", beanFactory == userRepository.getBeanFactory());

        log.info("beanFactory == userRepository.getObjectFactory().getObject() ? {}", beanFactory == userRepository.getObjectFactory().getObject());

//      System.out.println(userRepository.getUsers());

        // 依赖来源二：依赖注入（內建依赖）

//        ObjectFactory userFactory = userRepository.getObjectFactory();
//
//        System.out.println(userFactory.getObject() == applicationContext);

        // 依赖查找（错误）
//        System.out.println(beanFactory.getBean(BeanFactory.class));

        // 依赖来源三：容器內建 Bean
//        Environment environment = applicationContext.getBean(Environment.class);
//        System.out.println("获取 Environment 类型的 Bean：" + environment);
    }

    private static void whoIsIoCContainer(UserRepository userRepository, ApplicationContext applicationContext) {


        // ConfigurableApplicationContext <- ApplicationContext <- BeanFactory

        // ConfigurableApplicationContext#getBeanFactory()


        // 这个表达式为什么不会成立
        System.out.println(userRepository.getBeanFactory() == applicationContext);

        // ApplicationContext is BeanFactory

    }

}
